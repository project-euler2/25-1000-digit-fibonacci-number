def nth_digit_fibonacci_number(n):
    solution = False
    fibonacci_numbers = [1,1]
    i = 2
    while solution == False:
        fibonacci_numbers.append(fibonacci_numbers[i-2]+fibonacci_numbers[i-1])
        if len(str(fibonacci_numbers[i])) == n:
            solution = True
        i += 1
    return i

print(nth_digit_fibonacci_number(1000))